const config = {
  reelsCount: 5,
  rowsCount: 3,
  symbols: {
    1: [0, 0, 10, 20, 50],
    2: [0, 0, 20, 40, 100],
    3: [0, 0, 30, 60, 150],
    4: [0, 0, 40, 80, 200],
    5: [0, 0, 50, 100, 250],
    6: [0, 0, 100, 200, 500],
    7: [0, 0, 150, 300, 800],
    8: [0, 0, 200, 400, 1000],
    9: [0, 0, 300, 600, 2000],
  },
  lines: [
    [0, 0, 0, 0, 0],
    [1, 1, 1, 1, 1],
    [2, 2, 2, 2, 2],
    [0, 1, 0, 1, 0],
    [1, 2, 1, 2, 1],
  ],
  reels: [
    [
      1, 1, 2, 2, 9, 9, 3, 3, 1, 1, 8, 8, 8, 3, 3, 6, 6, 1, 1, 7, 7, 2, 2, 6, 6,
      1, 1, 8, 8, 2, 2, 5, 5, 4, 4, 4, 1, 1, 4, 4, 2, 2, 3, 3, 4, 4, 9, 9, 3, 3,
      2, 2, 1, 1, 9, 9, 1, 1, 4, 4, 8, 8, 2, 2, 5, 5, 5, 3, 3, 1, 1, 7, 7, 3, 3,
      6, 6, 7, 7, 2, 2, 6, 6, 6, 1, 1, 8, 8, 2, 2, 7, 7, 5, 5, 5, 1, 1, 6, 6, 4,
      4, 3, 3, 4, 4, 5, 5, 3, 3, 2, 2, 1, 1, 1, 1, 2, 2, 9, 9, 3, 3, 1, 1, 8, 8,
      8, 3, 3, 6, 6, 1, 1, 7, 7, 2, 2, 6, 6, 1, 1, 8, 8, 2, 2, 5, 5, 4, 4, 4, 1,
      1, 4, 4, 2, 2,
    ],
    [
      1, 1, 5, 5, 3, 3, 1, 1, 7, 7, 7, 4, 4, 9, 9, 5, 5, 1, 1, 4, 4, 9, 9, 3, 3,
      6, 6, 7, 7, 2, 2, 6, 6, 6, 2, 2, 2, 3, 3, 4, 4, 8, 8, 8, 3, 3, 2, 2, 1, 1,
      4, 4, 1, 1, 8, 8, 2, 2, 5, 5, 1, 1, 5, 5, 9, 9, 3, 3, 1, 1, 7, 7, 4, 4, 5,
      5, 1, 1, 4, 4, 4, 4, 3, 3, 6, 6, 7, 7, 2, 2, 6, 6, 2, 2, 2, 3, 3, 4, 4, 3,
      3, 2, 2, 1, 1, 1, 1, 8, 8, 2, 2, 5, 5, 6, 6, 2, 2, 2, 3, 3, 4, 4, 3, 3, 2,
      2, 1, 1, 1, 1, 8, 8, 2, 2, 5, 5,
    ],
    [
      1, 1, 9, 9, 2, 2, 2, 5, 5, 8, 8, 3, 3, 1, 1, 7, 7, 3, 3, 6, 6, 7, 7, 2, 2,
      6, 6, 6, 1, 1, 8, 8, 2, 2, 5, 5, 4, 4, 4, 5, 5, 1, 1, 4, 4, 3, 3, 4, 4, 3,
      3, 2, 2, 9, 9, 1, 1, 1, 1, 2, 2, 2, 5, 5, 3, 3, 1, 1, 7, 7, 3, 3, 6, 6, 7,
      7, 2, 2, 6, 6, 6, 1, 1, 8, 8, 2, 2, 5, 5, 7, 7, 4, 4, 5, 5, 1, 1, 4, 4, 3,
      3, 4, 4, 3, 3, 9, 9, 2, 2, 1, 1, 6, 6, 6, 1, 1, 8, 8, 2, 2, 5, 5, 7, 7, 4,
      4, 5, 5, 1, 1, 4, 4, 3, 3, 4, 4, 3, 3, 9, 9, 2, 2, 1, 1,
    ],
    [
      1, 1, 8, 8, 8, 2, 2, 4, 4, 3, 3, 9, 9, 9, 2, 2, 2, 5, 5, 7, 7, 2, 2, 5, 5,
      3, 3, 1, 1, 7, 7, 3, 3, 6, 6, 6, 1, 1, 4, 4, 4, 5, 5, 5, 1, 1, 4, 4, 8, 8,
      3, 3, 6, 6, 2, 2, 1, 1, 9, 9, 1, 1, 8, 8, 2, 2, 4, 4, 3, 3, 2, 2, 2, 5, 5,
      5, 7, 7, 2, 2, 9, 9, 3, 3, 1, 1, 7, 7, 3, 3, 6, 6, 1, 1, 7, 7, 5, 5, 1, 1,
      4, 4, 3, 3, 8, 8, 6, 6, 2, 2, 1, 1, 9, 9, 3, 3, 1, 1, 7, 7, 3, 3, 6, 6, 1,
      1, 7, 7, 5, 5, 1, 1, 4, 4, 3, 3, 8, 8, 6, 6, 2, 2, 1, 1,
    ],
    [
      1, 1, 5, 5, 7, 7, 3, 3, 9, 9, 9, 1, 1, 3, 3, 2, 2, 2, 7, 7, 2, 2, 6, 6, 6,
      1, 1, 8, 8, 2, 2, 4, 4, 3, 3, 4, 4, 4, 5, 5, 1, 1, 6, 6, 4, 4, 8, 8, 3, 3,
      6, 6, 2, 2, 1, 1, 8, 8, 1, 1, 5, 5, 3, 3, 9, 9, 1, 1, 7, 7, 3, 3, 2, 2, 2,
      5, 5, 1, 1, 7, 7, 7, 2, 2, 6, 6, 6, 1, 1, 8, 8, 8, 2, 2, 4, 4, 3, 3, 5, 5,
      1, 1, 4, 4, 3, 3, 9, 9, 9, 6, 6, 2, 2, 1, 1, 2, 2, 6, 6, 6, 1, 1, 8, 8, 8,
      2, 2, 4, 4, 3, 3, 5, 5, 1, 1, 4, 4, 3, 3, 9, 9, 9, 6, 6, 2, 2, 1, 1,
    ],
  ],
};

class SlotGame {
  constructor(config) {
    this.config = config;
  }
  spin() {
    const { reelsCount, rowsCount, symbols, lines, reels } = this.config;

    // GENERATE RANDOM SPIN RESULT

    //create an array to store the result
    const spinResult = [];

    //iterate through rows to fill with symbols
    for (let row = 0; row < rowsCount; row++) {
      //make an array for the current row to store the symbols
      const spinRow = [];

      //iterate through reels arrays to get values from them for each symbol in the row with random
      for (let reel = 0; reel < reelsCount; reel++) {
        const randomIndex = Math.floor(Math.random() * reels[reel].length);
        spinRow.push(reels[reel][randomIndex]);
      }
      spinResult.push(spinRow);
    }

    // CALCULATE PAYOUT

    // create empty payouts array
    const payouts = [];
    // iterate through all lines to calculate payouts
    for (let i = 0; i < lines.length; i++) {
      const line = lines[i]; //create a variable to hold the current line
      let linePayout = 0; //create a variable for the calculated payout
      let prevSymbol = spinResult[line[0]][0]; //take the first symbol for comparison with the current symbol
      let count = 0; //create a count variable to store the number of consecutive symbols in the line

      //iterate through the spin result to look for consecutive symbols
      for (let j = 1; j < line.length; j++) {
        const symbol = spinResult[line[j]][j]; //take the next value needed for comparison

        //compare the current symbol with the previous if equal increase the number of consecutive symbols with 1
        if (symbol === prevSymbol) {
          count++; //if equal increase the number of consecutive symbols with 1
        } else {
          //if not equal we make a check if the count >= 3
          if (count >= 2) {
            //if true add the payout for the combination to line payout
            linePayout = symbols[prevSymbol][count];
          }
          //prevSymbol now stores the value of the current symbol and the counter is reset.
          prevSymbol = symbol;
          count = 0;
        }
      }

      //If the count of the consecutive symbols equals or is bigger than 2
      //the value for this combination is added to the line payout.
      if (count >= 2) {
        linePayout = symbols[prevSymbol][count];
      }

      //Push the payout for current line to the payouts array.
      payouts.push(linePayout);
    }

    // Print spin result and payouts
    console.log("Spin Result:");
    for (let row = 0; row < rowsCount; row++) {
      console.log(spinResult[row].join(" "));
    }
    console.log("\nPayouts:");
    for (let i = 0; i < lines.length; i++) {
      console.log(`Line ${i + 1}: ${payouts[i]}`);
    }
  }
}

const slotGame = new SlotGame(config);
slotGame.spin();
